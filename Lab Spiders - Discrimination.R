setwd("C:/Users/jeffmark/Local Business/SIA-Lab-Project-copy/csv Data/")
rm(list=ls()) # clear environment
dev.off() # Clear plots

library(emmeans)

# Discrimination Value Analysis #

## Load and transform data ##

all.frac.aov <- read.csv("SIA_Lab_R_comprehensive.csv")
all.frac.aov <- na.omit(all.frac.aov) # remove base food source and molts
names(all.frac.aov)[names(all.frac.aov) == "d2H_non_exchangeable"] <- "d2H_ex"
ter.sub <- c("Terrestrial")
aq.sub <- c("Aquatic")
tr.all.frac <- subset(all.frac.aov, Category %in% ter.sub)
aq.all.frac <- subset(all.frac.aov, Category %in% aq.sub)

## Terrestrial Group ##
# d15N #
tr.all.frac.aov.N <- aov(d15N ~ Tissue, data = tr.all.frac)
summary(tr.all.frac.aov.N)
tr.all.emmeans.N <- emmeans(tr.all.frac.aov.N, pairwise ~ Tissue)
tr.all.emmeans.N

# d13C #
tr.all.frac.aov.C <- aov(d13C ~ Tissue, data = tr.all.frac)
summary(tr.all.frac.aov.C)
tr.all.emmeans.C <- emmeans(tr.all.frac.aov.C, pairwise ~ Tissue)
tr.all.emmeans.C

#d2H
tr.all.frac.aov.H <- aov(d2H_ex ~ Tissue, data = tr.all.frac)
summary(tr.all.frac.aov.H)
tr.all.emmeans.H <- emmeans(tr.all.frac.aov.H, pairwise ~ Tissue)
tr.all.emmeans.H

## Aquatic Group ##
# d15N #
aq.all.frac.aov.N <- aov(d15N ~ Tissue, data = aq.all.frac)
summary(aq.all.frac.aov.N)
aq.all.emmeans.N <- emmeans(aq.all.frac.aov.N, pairwise ~ Tissue)
aq.all.emmeans.N

#d13C
aq.all.frac.aov.C <- aov(d13C ~ Tissue, data = aq.all.frac)
summary(aq.all.frac.aov.C)
aq.all.emmeans.C <- emmeans(aq.all.frac.aov.C, pairwise ~ Tissue)
aq.all.emmeans.C

#d2H
aq.all.frac.aov.H <- aov(d2H_ex ~ Tissue, data = aq.all.frac)
summary(aq.all.frac.aov.H)
aq.all.emmeans.H <- emmeans(aq.all.frac.aov.H, pairwise ~ Tissue)
aq.all.emmeans.H
