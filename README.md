# Non-lethal sampling for stable isotopes project

This repository contains R code, data sets, raw isotopic data, and supplemental material used to
investigate non-lethal sampling methods on spiders for stable isotope analysis.

File descriptions:

**csv Data folder**:

- **Denis.spider.data.LONG.csv**: d13C and d15N isotopic data of legs and abdomens from field-collected spiders in a LONG data format
- **Denis.spider.data.csv**: same as above in WIDE data format
- **SIA_Lab_R_Comprehensive**: d13C, d15N, and d2H (measured and non-exchangeable) isotopic data of lab-reared spider tissues
- **Samples_by_C_N.csv**: same as above in WIDE data format

**Supplementary Material**:

- **Iso-Analytical_Report_Marker_KAU_.docx**: SOPs and methods for isotopic analysis from Iso-Analytical Limited
- **SIA_Lab_-_Raw_Data-xlsx**: Unmodified, raw isotopic data from lab-reared spiders

**Main Repository**:

**R scripts**:

- **Field Spiders - Supplementary Material.R**: R script for field-collected spiders not included in main manuscript
- **Field Spiders.R**: Descriptive statistics, regression analysis, and plots for field-collected spiders
- **Lab Spiders - Discriminaton.R**: ANOVA and emmeans analysis of isotopic discriminaton values for lab-reared spiders
- **Lab Spiders - Molts**: Box plots for molt analysis
- **Lab Spiders - Plot**: Plots for lab-reared spider tissue analysis
- **Lab Spiders - Regression**: Main regression and 1:1 fit line analysis for lab-reared spider tissues

# Related publication:

- Manuscript: Marker, Jeffery, Denis Lafage, Eva Bergman, and Rachel E. Bowes. 2022. “Greater than the Sum of Your Parts: Nonlethal Stable      Isotope Sampling Methods in Spiders.” Ecosphere 13(1): e3903. https://doi.org/10. 1002/ecs2.3903
